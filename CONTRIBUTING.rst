Contribution Guide
==================

Filing a bug report or feature request
--------------------------------------

.. rubric:: Via GitLab

If you have a GitLab account, just head to PICOS' official
`issue tracker <https://gitlab.com/picos-api/picos/issues>`_.

.. rubric:: Via mail

If you don't have a GitLab account you can still create an issue by writing to
`incoming+picos-api/picos@incoming.gitlab.com
<incoming+picos-api/picos@incoming.gitlab.com>`_. Unlike issues created directly
on GitLab, issues created by mail are *not* publicly visible.

Submitting a code change
------------------------

The canoncial way to submit a code change is to

1. fork the `PICOS repository on GitLab <https://gitlab.com/picos-api/picos>`_,
2. clone your fork and make your application use it instead of your system's
   PICOS installation,
3. optionally create a local topic branch to work with,
4. modify the source and commit your changes, and lastly
5. make a pull request on GitLab so that we can test and merge your changes.

Code style
----------

Set your linter to enforce :pep:`8` and :pep:`257` except for the following
codes:

.. code::

    D105,D203,D213,D401,E122,E128,E221,E271,E272,E501,E702,E741

Our line width limit is ``80`` characters.

Implementing a test case
------------------------

Production test sets are implemented in the files in the :file:`tests` folder
that start with :file:`ptest_`. If you want to add to our test pool, feel free
to either extend these files or create a new set, whatever is appropriate. Make
sure that the tests you add are not too computationally expensive since they are
also run as part of our continuous integration pipeline whenever a commit is
pushed to GitLab.

Implementing a solver
---------------------

If you want to implement support for a new solver, all you have to do is update
:file:`solvers/__init__.py` where applicable, and add a file named
:file:`solver_<name>.py` in the same directory with your implementation. We
recommend that you read two or three of the existing solver implementations to
get an idea how things are done. If you want to know exactly how PICOS
communicates with your implementation, refer to the solver base class in
:file:`solver.py`.
