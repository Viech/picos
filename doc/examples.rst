.. _examples:

Examples
========

.. toctree::
   :maxdepth: 2

   graphs
   complex
   optdes
   constraints
