.. _userguide:

Usage Notes
===========

.. toctree::
   :maxdepth: 2

   slicing.rst
   duals.rst
   tolerances.rst
   cheatsheet.rst
