#!/usr/bin/python

# ------------------------------------------------------------------------------
# Copyright (C) 2012-2017 Guillaume Sagnol
# Copyright (C) 2018-2021 Maximilian Stahlberg
#
# This file is part of PICOS Release Scripts.
#
# PICOS Release Scripts are free software: you can redistribute them and/or
# modify them under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# PICOS Release Scripts are distributed in the hope that they will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

"""Packaging and installation script for PICOS."""

from setuptools import find_packages, setup

setup(
    name="PICOS",
    use_scm_version={"write_to": "picos/version.py"},
    description="A Python interface to conic optimization solvers.",
    long_description=open("README.rst").read(),
    long_description_content_type="text/x-rst",
    author="G. Sagnol, M. Stahlberg",
    author_email="incoming+picos-api/picos@incoming.gitlab.com",
    classifiers=[
        "Topic :: Scientific/Engineering :: Mathematics",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
    ],
    keywords=[
        "conic optimization",
        "convex optimization",
        "robust optimization",
        "semidefinite programming",
    ],
    project_urls={
        "Source": "https://gitlab.com/picos-api/picos",
        "Documentation": "https://picos-api.gitlab.io/picos/",
    },
    packages=find_packages(include=("picos", "picos.*")),
    setup_requires=["setuptools_scm"],
    install_requires=["cvxopt", "numpy"],
    python_requires=">=3.4",
)
